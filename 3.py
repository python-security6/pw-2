from typing import List

def tail(rows, n=5):
    for row in rows[-n:]:
        print(row)

rows: List[str]= [
 'first',
 'second',
 'third',
 'fourth',
 'fifth',
 'sixth',
 'seven',
 'eight',
 'nine',
 'ten',
 ]
n: int=3

tail(rows, n)