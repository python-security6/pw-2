def print_logs(func):
    def foo(*args,**kwargs):
        print (func(*args,**kwargs))
    return foo

@print_logs
def say_hello(name:str):
    return 'hello, {name}'.format(name=name)

@print_logs
def double_it(x:int):
    return x * 2

say_hello('roma')
double_it(5)
