from functools import singledispatch

@singledispatch
def double_it(arg):
    return 'Тип не знаю! Значение: {arg}'.format(arg=arg)

@double_it.register
def _(arg:str) -> str:
    output = ""
    for ar in arg:
        output += ar*2
    return output

@double_it.register
def _(arg: int) -> int:
    return arg*2

@double_it.register
def _(arg: float) -> float:
    return arg*2

pattern = '{} output: {}'
print(pattern.format(str, double_it('hi')))
print(pattern.format(int, double_it(1)))
print(pattern.format(float, double_it(3.5)))
print(pattern.format(list, double_it([])))