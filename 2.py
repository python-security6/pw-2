from functools import wraps

def print_logs(func):
    @wraps(func)
    def foo(*args,**kwargs):
        print (func(*args,**kwargs))
    return foo

@print_logs
def say_hello(name:str):
    return 'function "say_hello" result is: hello, {name}'.format(name=name)

@print_logs
def double_it(x:int):
    """double any instance"""
    return 'function "double_it" result is: {x}'.format(x=x*2)

say_hello('roma')
double_it(5)

print(say_hello.__name__)
print(double_it.__doc__)