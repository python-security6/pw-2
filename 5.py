from functools import wraps

def counter(func):
    @wraps(func)
    def foo(*args,**kwargs):
        print ('function <'+func.__name__+'> calls count - 1')
    return foo

@counter
def say_hello(name: str):
    return 'hello, {name}'.format(name=name)
@counter
def double_it(x):
    """double any instance"""
    return x * 2

double_it(4)
double_it(6)
say_hello('roma')
double_it(1)